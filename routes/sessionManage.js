// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();


const app = express.Router();

app.use(express.json({ limit:'5mb'}));

app.get('/api/session/getDetails',async (req, res) => {
    const { id } = req.auth;
    const User = await prisma.user.findUnique({
        where: {
            id: id
        }
    });
    delete User.id;
    delete User.password;
    delete User.lastCheckIn;
    delete User.registerAt;
    res.send({
        status: 200,
        message: 'OK',
        data: User
    });
});

module.exports = app;