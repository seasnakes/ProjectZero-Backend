// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const verifyCaptcha = require('../modules/captcha');
const emailSender = require('../modules/emailSender');
const emailValidator = require('../modules/emailValidator');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const saltRounds = Number(process.env.SALT_ROUNDS);

const app = express.Router();

app.use(express.json({ limit:'5mb'}));

app.post('/api/account/login', async (req, res) => {
    const { email, password } = req.body;
    if(email && password) {
        const User = await prisma.user.findUnique({
            where: {
                email: email
            },
            include: {
                profile: true
            }
        });
        if(User) {
            if(bcrypt.compareSync(password, User.password)) {
                const token = jwt.sign({
                    id: User.id,
                    email: email,
                    role: User.role
                }, process.env.JWT_SECRET, { expiresIn: '7d' });
                req.body.jwt = token;
                await eventLogger.newEvent('userLogin', {
                    ip: req.headers['x-forwarded-for'],
                    auth: req.auth,
                    body: req.body
                });
                res.send({
                    status: 200,
                    message: 'OK',
                    token: token
                });
            }
            else res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/register', async (req, res) => {
    const { email, password, token, type } = req.body;
    if(email && password && token && type) {
        if(await verifyCaptcha(token, type)) {
            await eventLogger.newEvent('userRegister', {
                ip: req.headers['x-forwarded-for'],
                auth: req.auth,
                body: req.body
            });
            if(!emailValidator.testEmail(email)) {
                res.send({
                    status: 400,
                    message: 'Invalid email'
                });
                return;
            }
            const cUser = await prisma.user.findUnique({
                where: {
                    email: email
                }
            });
            if(cUser) {
                res.send({
                    status: 400,
                    message: 'This email is already registered'
                });
                return;
            }
            await prisma.user.create({
                data: {
                    email: email,
                    password: bcrypt.hashSync(password, bcrypt.genSaltSync(saltRounds)),
                    registerAt: new Date().getTime()
                }
            });
            const User = await prisma.user.findUnique({
                where: {
                    email: email
                },
                include: {
                    profile: true
                }
            });
            const token = jwt.sign({
                id: User.id,
                email: email,
                role: User.role
            }, process.env.JWT_SECRET, { expiresIn: '7d' });
            res.send(
                {
                    status: 200,
                    message: 'OK',
                    token: token
                }
            );
            const url = process.env.EMAIL_VERIFICATION_PAGE + '?email=' + encodeURIComponent(email) + '&uuid=' + await emailValidator.newEmailVerification(email);
            emailSender.sendEmail(email, 'Kamiya Email Verification', emailSender.getVerificationEmail(url));
        }
        else {
            res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/changeEmail', async (req, res) => {
    const { id, email } = req.auth;
    const { password, newEmail } = req.body;
    if(password && newEmail) {
        const User = await prisma.User.findUnique({
            where: {
                email: email
            },
            include: {
                profile: true
            }
        });
        if(User) {
            if(bcrypt.compareSync(password, User.password)) {
                if(emailValidator.testEmail(newEmail)) {
                    await prisma.User.update({
                        where: {
                            email: email
                        },
                        data: {
                            email: newEmail,
                            active: false
                        }
                    });
                    const newUser = await prisma.User.findUnique({
                        where: {
                            id: id
                        },
                        include: {
                            profile: true
                        }
                    });
                    const token = jwt.sign({
                        id: newUser.id,
                        email: newUser.email,
                        role: newUser.role
                    }, process.env.JWT_SECRET, { expiresIn: '7d' });
                    res.send({
                        status: 200,
                        message: 'OK',
                        token: token
                    });
                }
                else res.send({
                    status: 400,
                    message: 'Invalid email'
                });
            }
            else res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/changePassword', async (req, res) => {
    const { email } = req.auth;
    const { oldPassword, newPassword } = req.body;
    if(email && oldPassword && newPassword) {
        const User = await prisma.User.findUnique({
            where: {
                email: email
            },
            include: {
                profile: true
            }
        });
        if(User) {
            if(bcrypt.compareSync(oldPassword, User.password)) {
                await prisma.User.update({
                    where: {
                        email: email
                    },
                    data: {
                        password: bcrypt.hashSync(newPassword, bcrypt.genSaltSync(saltRounds))
                    }
                });
                res.send(kamiyaToolKit.baseResponse.ok);
            }
            else res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
        else res.send(kamiyaToolKit.baseResponse.unAuthorized);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/requestPasswordReset', async (req, res) => {
    const { email, token, type } = req.body;
    if(email && token && type) {
        if(await verifyCaptcha(token, type)) {
            const User = await prisma.user.findUnique({
                where: {
                    email: email
                },
                include: {
                    profile: true
                }
            });
            if(User) {
                const url = process.env.PASSWORD_RESET_PAGE + '?email=' + email + '&uuid=' + await emailValidator.newPasswordReset(email);
                emailSender.sendEmail(email, 'Kamiya Password Reset', emailSender.getPasswordResetEmail(url));
                res.send(kamiyaToolKit.baseResponse.ok);
            }
        }
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/resetPassword', async (req, res) => {
    const { email, uuid, password } = req.body;
    if(email && uuid && password) {
        if(await emailValidator.checkPasswordReset(uuid, email)) {
            await prisma.user.update({
                where: {
                    email: email
                },
                data: {
                    password: bcrypt.hashSync(password, bcrypt.genSaltSync(saltRounds))
                }
            });
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else {
            res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/requestEmailVerification', async (req, res) => {
    const { email } = req.auth;
    const { token, type } = req.body;
    if(email && token && type) {
        if(await verifyCaptcha(token, type)) {
            const User = await prisma.user.findUnique({
                where: {
                    email: email
                },
                include: {
                    profile: true
                }
            });
            if(User && !User.active) {
                const url = process.env.EMAIL_VERIFICATION_PAGE + '?email=' + encodeURIComponent(email) + '&uuid=' + await emailValidator.newEmailVerification(email);
                emailSender.sendEmail(email, 'Kamiya Email Verification', emailSender.getVerificationEmail(url));
                res.send(kamiyaToolKit.baseResponse.ok);
            }
            else res.send(kamiyaToolKit.baseResponse.badRequest);
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/account/verifyEmail', async (req, res) => {
    const { email, uuid } = req.body;
    if(email && uuid) {
        if(await emailValidator.checkEmailVerification(uuid, email)) {
            await prisma.user.update({
                where: {
                    email: email
                },
                data: {
                    active: true
                }
            });
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else {
            res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

module.exports = app;