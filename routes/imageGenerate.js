// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const uploadImage = require('../modules/imageUploader');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');
const request = require('then-request');

const { PrismaClient } = require('@prisma/client');
const fs = require("fs");
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

let traceCache = {};

app.get('/api/image/listBaseModel',(req,res) => {
    res.send(JSON.parse(fs.readFileSync('./data/modelConfig.json').toString()).baseModel);
});
app.get('/api/image/listLoraModel',(req,res) => {
    res.send(JSON.parse(fs.readFileSync('./data/modelConfig.json').toString()).loraModel);
});

app.get('/api/image/trace',async (req,res) => {
    const { id } = req.query;
    if(id && traceCache[id]) {
        request('GET',traceCache[id] + '/sdapi/v1/progress').getBody('utf8').then(JSON.parse).done(function(response) {
            res.send({
                status: 200,
                message: 'OK',
                progress: response.progress,
            });
        },(err) =>{
            console.error(err);
            res.send(kamiyaToolKit.baseResponse.internalError);
        });
    }
});

app.post('/api/image/generate', async (req, res) => {
    const { id } = req.auth;
    const { traceId } = req.body;
    const User = await prisma.User.findUnique({
        where: {
            id: id
        }
    });
    if(!User.active || !(User.credit > 0)) {
        res.send({
            status: 403,
            message: 'Forbidden, 用户未激活或魔晶不足'
        });
        return;
    }
    const config = {
        prompt: req.body.prompt,
        negativePrompt: req.body.negativePrompt,
        steps: Number(req.body.steps),
        scale: Number(req.body.scale),
        seed: Number(req.body.seed) || 0,
        sampler: req.body.sampler,
        width: Number(req.body.width),
        height: Number(req.body.height),
        image: req.body.image,
        model: req.body.model
    };

    if(kamiyaToolKit.checkImageGenerateConfig(config)) {
        let cost = 100;

        const resolution = kamiyaToolKit.customResolution(config.width, config.height);
        config.width = resolution.width;
        config.height = resolution.height;

        if(config.image) cost = cost * 2;
        if(config.width * config.height > 768 * 512) cost = cost * 2;

        const webuiNode = kamiyaToolKit.pickWebuiNode(config.model);
        const webuiRequest = kamiyaToolKit.createWebuiRequest(config, webuiNode);

        request('POST', webuiNode[0].url + '/run/predict/', {json: webuiRequest.predictBody}).done(() => {
            if(traceId) traceCache[traceId] = webuiNode[0].url;
            request('POST',webuiNode[0].url + webuiRequest.path, {json: webuiRequest.requestBody}).getBody('utf8').then(JSON.parse).then(
                async (response) => {
                    const uploaded = await uploadImage('data:image/png;base64,' + response.images[0]);
                    res.send({
                        status: 200,
                        message: 'OK',
                        image: uploaded.url,
                        backend: webuiNode[0].name
                    });
                    if(!uploaded.success) uploaded.url = 'Failed to upload this content,contact the admin.';
                    await prisma.User.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: cost
                            },
                            images: {
                                create: {
                                    uuid: uploaded.uuid,
                                    url: uploaded.url
                                }
                            }
                        }
                    });
                    await eventLogger.newEvent('imageGenerate', {
                        ip: req.headers['x-forwarded-for'],
                        auth: req.auth,
                        body: req.body,
                        uploaded: uploaded
                    });
                }, (err) => {
                    console.error(err);
                    res.send({
                        status: 500,
                        message: 'Internal Server Error, 后端API错误 '
                    });
                }
            );
        }, (err) => {
            console.error(err);
            res.send({
                status: 500,
                message: 'Internal Server Error, 后端API错误 '
            });
        });
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

module.exports = app;

