// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const kamiyaToolKit = require('../modules/toolKit');
const request = require('then-request');
const verifyCaptcha = require('../modules/captcha');

const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

app.post('/api/mirateAccount/apply', async (req, res) => {
    const { id } = req.auth;
    const { token, captchaToken, type } = req.body;
    if(!await verifyCaptcha(captchaToken, type)) {
        res.send(kamiyaToolKit.baseResponse.badRequest);
        return;
    }
    if(token) {
        let oldKamiyaResponse = await request('POST', process.env.OLD_KAMIYA_API + '/api/migrateAccount/getDetails', {
            json: {token: token, adminPass: process.env.OLD_KAMIYA_ADMINPASS }
        });
        oldKamiyaResponse = JSON.parse(oldKamiyaResponse.getBody('utf8'));
        if(oldKamiyaResponse.success) {
            await prisma.User.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        increment: oldKamiyaResponse.credit
                    }
                }
            });
            let userContent = oldKamiyaResponse.userContent;
            if(userContent) {
                await prisma.Image.createMany({
                    data: userContent.map((item) => {
                        return {
                            uuid: item.uuid,
                            url: item.url,
                            authorId: id
                        }
                    })
                });
            }
            await prisma.Bind.create(
                {
                    data: {
                        userId: id,
                        platform: oldKamiyaResponse.bind.platform,
                        bindId: oldKamiyaResponse.bind.bindId
                    }
                }
            )
            await request('POST', process.env.OLD_KAMIYA_API + '/api/migrateAccount/finishMigrate', {
                json: {token: token, adminPass: process.env.OLD_KAMIYA_ADMINPASS }
            });
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else res.send(kamiyaToolKit.baseResponse.notFound);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

module.exports = app;