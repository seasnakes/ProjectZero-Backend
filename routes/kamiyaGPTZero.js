// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const { client } = require('../modules/redisClient');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');
const request = require('then-request');

const { PrismaClient } = require('@prisma/client');
const fs = require("fs");
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

let accessToken;

async function initGPTZeroSession() {
    const account = JSON.parse(fs.readFileSync('./data/backend.json').toString()).gptzero;
    const session = await client.get('GPTZeroSession');
    if(session) accessToken = session;
    else {
        accessToken = JSON.parse((await request('POST', 'https://lydqhgdzhvsqlcobdfxi.supabase.co/auth/v1/token?grant_type=password', {
            headers: { 'apikey': account.apiKey },
            json: { 'email': account.email, 'password': account.password }
        }).getBody()).toString()).access_token;
        await client.set('GPTZeroSession', accessToken);
        await client.expire('GPTZeroSession', 604500);
    }
}

app.post('/api/gptzero/predict', async (req, res) => {
    const { id } = req.auth;
    const { document } = req.body;
    if(document.length <= 5000) {
        const User = await prisma.user.findUnique({
            where: {
                id: id
            }
        });
        if(!User.active || !(User.credit > 0)) {
            res.send({
                status: 403,
                message: 'Forbidden, 用户未激活或魔晶不足'
            });
            return;
        }
        await initGPTZeroSession();
        try {
            const response = JSON.parse((await request('POST', 'https://api.gptzero.me/v2/predict/text', {
                headers: { 'Cookie': 'accessToken=' + accessToken },
                json: { document: document }
            }).getBody()).toString());
            await prisma.user.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        decrement: response.documents[0].sentences.length * 15
                    }
                }
            });
            await eventLogger.newEvent('GPTZeroPredict',{
                auth: req.auth,
                document: document
            });
            res.send({
                status: 200,
                message: 'OK',
                documents: response.documents
            });
        }
        catch (e) {
            console.log(e);
            res.send(kamiyaToolKit.baseResponse.internalError);
        }

    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

module.exports = app;