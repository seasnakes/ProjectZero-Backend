// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const verifyCaptcha = require('../modules/captcha');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'5mb'}));

app.post('/api/billing/checkin', async (req, res) => {
    const { id } = req.auth;
    const { token, type } = req.body;
    if(token && type) {
        if(await verifyCaptcha(token, type)) {
            const User = await prisma.user.findUnique({
                where: {
                    id: id
                }
            });
            if(User) {
                if(kamiyaToolKit.dateFormat('YYYY-mm-dd',new Date()) !== kamiyaToolKit.dateFormat('YYYY-mm-dd', new Date(Number(User.lastCheckIn)))) {
                    await eventLogger.newEvent('userCheckin',{
                        auth: req.auth
                    });
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            lastCheckIn: new Date().getTime(),
                            credit: {
                                increment: 7500
                            }
                        }
                    });
                    res.send({
                        status: 200,
                        message: '获得 75 魔晶'
                    });
                }
                else {
                    res.send({
                        status: 400,
                        message: '今天已经签到了'
                    });
                }
            }
            else res.send(kamiyaToolKit.baseResponse.unAuthorized);
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.badRequest);
});

app.post('/api/billing/addCredit', async (req, res) => {
   const { adminPass, credit, id, email } = req.body;
    if(adminPass === process.env.ADMINPASS) {
        if(credit) {
            if(email) {
                await prisma.user.update({
                    where: {
                        email: email
                    },
                    data: {
                        credit: {
                            increment: Number(credit) * 100
                        }
                    }
                });
                res.send(kamiyaToolKit.baseResponse.ok);
                return;
            }
            await prisma.user.update({
                where: {
                    id: id
                },
                data: {
                    credit: {
                        increment: Number(credit) * 100
                    }
                }
            });
            res.send(kamiyaToolKit.baseResponse.ok);
        }
        else res.send(kamiyaToolKit.baseResponse.badRequest);
    }
    else res.send(kamiyaToolKit.baseResponse.unAuthorized);
});

module.exports = app;