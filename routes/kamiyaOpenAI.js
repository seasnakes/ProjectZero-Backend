// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');
const { client: redisClient } = require('../modules/redisClient');
const { v4: uuid } = require('uuid');
const kamiyaToolKit = require('../modules/toolKit');
const eventLogger = require('../modules/eventLogger');
const tokenizer = require('gpt-3-encoder');

const { Configuration, OpenAIApi } = require('openai');
const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

const { PrismaClient } = require('@prisma/client');
const fs = require("fs");
const prisma = new PrismaClient();

const app = express.Router();

app.use(express.json({ limit:'100mb'}));

function sizeContent(t) {
    return tokenizer.encode(t).length;
}

function processContext(c) {
    total = 0;
    for(let i in c) {
        total += sizeContent(c[i].content);
    }
    if(total < 4095) return c;
    else {
        let i = 4;
        while(total > 4095) {
            total -= sizeContent(c[i].content);
            c.splice(i,1);
            i++;
        }
        return c;
    }
}

const ChatGPTHandler = async (req, res) => {
    const {id} = req.auth;
    let {conversationId, content, role} = req.body;
    if (content) {
        const User = await prisma.User.findUnique({
            where: {
                id: id
            }
        });
        if (!User.active || !(User.credit > 0)) {
            res.send({
                status: 403,
                message: 'Forbidden, 用户未激活或魔晶不足'
            });
            return;
        }
        let context;

        if (conversationId) context = JSON.parse(await redisClient.get(id + '.' + conversationId)) || kamiyaToolKit.OpenAI.defaultRole();
        else {
            context = kamiyaToolKit.OpenAI.defaultRole();
            if (role) {
                const roleList = JSON.parse(fs.readFileSync('./data/roleList.json').toString());
                if (roleList[role]) {
                    context.push({'role': 'user', 'content': roleList[role].user});
                    context.push({'role': 'assistant', 'content': roleList[role].assistant});
                }
            }
            conversationId = uuid();
        }

        context.push({'role': 'user', 'content': content});

        context = processContext(context);

        openai.createChatCompletion({
            model: 'gpt-3.5-turbo',
            messages: context,
            stream: true
        }, {
            responseType: 'stream'
        }).then((R) => {
            //console.log(R);
            res.set({
                'Cache-Control': 'no-cache',
                'Content-Type': 'text/event-stream',
                'Connection': 'keep-alive'
            });
            res.flushHeaders();
            let result = '', lock = false;
            R.data.on('data', async (chunk) => {
                if (chunk.toString().match(/data: \[DONE\]/)) {
                    res.write(chunk.toString().replace('\[DONE\]', JSON.stringify({
                        id: 'kamiyaInfo',
                        conversationId: conversationId,
                        message: '[DONE]',
                        fullContent: result
                    })));
                    await prisma.user.update({
                        where: {
                            id: id
                        },
                        data: {
                            credit: {
                                decrement: 100
                            }
                        }
                    });
                    if (lock) return;
                    context.push({'role': 'assistant', 'content': result});
                    await eventLogger.newEvent('chatMessage', {
                        conversationId: conversationId,
                        APIResponse: result,
                        requestBody: req.body
                    });
                    await redisClient.del(id + '.' + conversationId);
                    await redisClient.set(id + '.' + conversationId, JSON.stringify(context));
                    await redisClient.expire(id + '.' + conversationId, 60 * 60 * 24);
                    return;
                }
                try {
                    const J = JSON.parse(chunk.toString().replace('data: ', ''));
                    if (J.choices[0].delta) {
                        if (J.choices[0].delta.content) result += J.choices[0].delta.content;
                        if (result.match((/\[BLOCKED\]/))) {
                            await redisClient.del(conversationId);
                            lock = true;
                            return;
                        }
                    }
                } catch (e) {
                }
                res.write(chunk.toString());
            });
            R.data.on('end', () => {
                res.end();
            });
            req.on('close', () => {
            });
        }, async (e) => {
            console.log(e.response.data.error);
            res.send({
                status: '500',
                conversationId: conversationId,
                message: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversationId
            });
            await eventLogger.newEvent('chatMessage', {
                conversationId: conversationId,
                APIResponse: e.response.data.error.message + '，将此ID上报以快速定位此次错误' + conversationId,
                requestBody: req.body
            });
        });
    } else res.send(kamiyaToolKit.baseResponse.badRequest);
};

app.post('/api/openai/chatgpt/conversation', ChatGPTHandler);

app.post('/api/openai/knitgpt/conversation', ChatGPTHandler);

module.exports = app;