// noinspection JSUnresolvedVariable,DuplicatedCode

require('dotenv').config();

const express = require('express');

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const app = express.Router();

app.get('/api/userContent/get',async (req,res) => {
    const { start, take } = req.query;
    const { id } = req.auth;
    const Image = await prisma.Image.findMany({
        where: {
            authorId: id
        },
        select: {
            uuid: true,
            url: true
        },
        orderBy: {
            id: 'desc'
        },
        skip: Number(start) - 1,
        take: Number(take)
    });
    res.send({
        status: 200,
        message: 'OK',
        data: Image
    });
});

module.exports = app;