const { v4: uuid } = require('uuid');

const { client } = require('./redisClient');

const newPasswordReset = async (email) => {
    const u = uuid();
    await client.set(u + '.reset', email);
    await client.expire(u + '.reset', 60 * 60 * 72);
    return u;
}

const newEmailVerification = async (email) => {
    const u = uuid();
    await client.set(u + '.verify', email);
    await client.expire(u + '.verify', 60 * 60 * 72);
    return u;
}

const checkPasswordReset = async (uuid, email) => {
    const e = await client.get(uuid + '.reset');
    if(e === email) {
        await client.del(uuid + '.reset');
        return true;
    }
    return false;
}

const checkEmailVerification = async (uuid, email) => {
    const e = await client.get(uuid + '.verify');
    if(e === email) {
        await client.del(uuid + '.verify');
        return true;
    }
    return false;
}

const testEmail = (email) => {
    const reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
    return reg.test(email);
}

module.exports = {
    newPasswordReset: newPasswordReset,
    newEmailVerification: newEmailVerification,
    checkPasswordReset: checkPasswordReset,
    checkEmailVerification: checkEmailVerification,
    testEmail: testEmail
}