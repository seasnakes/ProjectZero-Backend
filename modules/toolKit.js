// noinspection JSUnresolvedVariable,DuplicatedCode

const fs = require("fs");

function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),
        "m+": (date.getMonth() + 1).toString(),
        "d+": date.getDate().toString(),
        "H+": date.getHours().toString(),
        "M+": date.getMinutes().toString(),
        "S+": date.getSeconds().toString()
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length === 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")));
        }
    }
    return fmt;
}

function pickWebuiNode (name) {
    const webuiL = JSON.parse(fs.readFileSync('./data/backend.json').toString()).webui;
    let cacheL = [];
    for(let i in webuiL) {
        const cache = JSON.parse(fs.readFileSync('./data/backendCache/' + webuiL[i].id + '.json').toString());
        cache.webui = webuiL[i];
        if(cache.online) cacheL.push(cache);
    }
    let model;
    let cacheH = [];
    for(let i in cacheL) {
        for(let j in cacheL[i].checkpoints) {
            if(cacheL[i].checkpoints[j].model_name === name) {
                model = cacheL[i].checkpoints[j].title;
                cacheL[i].model = model;
                cacheH.push(cacheL[i]);
            }
        }
    }
    if(cacheH.length < 1) return pickWebuiNode('anything-v4.0-fp16-default');
    if(!model) return pickWebuiNode('anything-v4.0-fp16-default');
    const r = Math.floor(Math.random() * cacheH.length);
    return [cacheH[r].webui,cacheH[r].model];
}

const kamiyaToolKit = {
    baseResponse: {
        notFound: {
            status: 404,
            message: 'Not Found'
        },
        badRequest: {
            status: 400,
            message: 'Bad Request'
        },
        forbidden: {
            status: 403,
            message: 'Forbidden'
        },
        unAuthorized: {
            status: 401,
            message: 'Unauthorized'
        },
        internalError: {
            status: 500,
            message: 'Internal Server Error'
        },
        ok: {
            status: 200,
            message: 'OK'
        }
    },
    OpenAI: {
        defaultRole: () => {
            return [
                {
                    'role': 'system',
                    'content': '你是由OpenAI开发的人工智能助手，现在的时间是' + dateFormat("YYYY-mm-dd HH:MM:SS", new Date())
                },
                {'role': 'user', 'content': '你是谁？'},
                {'role': 'assistant', 'content': '你好，我是由OpenAI创造的人工智能。'}
            ];
        }
    },
    checkImageGenerateConfig: function (config) {
        const check1 = /^[0-9]+.?[0-9]*/;
        const sampler = ['Euler a','Euler','LMS','PLMS','DDIM','DPM++ 2M','DPM++ 2M Karras','DPM adaptive'];
        //const wh = ['landscape','portrait','square','landscape_l','portrait_l','square_l','custom','image'];
        if(check1.test(config.steps) && check1.test(config.scale) && check1.test(config.seed) && check1.test(config.width) && check1.test(config.height)) {
            if((sampler.indexOf(config.sampler) !== -1)) {
                return true;
            }
        }
        return false;
    },
    pickWebuiNode: pickWebuiNode,
    customResolution: function (width, height ,customMax) {
        const maxResolution = customMax || 1024; //Max Square Resolution
        const s = [width, height];
        if(s.length === 2) {
            //console.log(s);
            let r = {};
            if(s[0] * 1 > s[1] * 1) {
                r.width = maxResolution;
                r.height = Math.floor(maxResolution * (s[1]/s[0]));
                let ad = 0;
                while (ad < r.height) {
                    ad += 64;
                }
                r.height = ad;
            }
            else {
                r.width = Math.floor(maxResolution * (s[0]/s[1]));
                r.height = maxResolution;
                let ad = 0;
                while (ad < r.width) {
                    ad += 64;
                }
                r.width = ad;
            }
            if(s[0] < maxResolution && s[1] < maxResolution) {
                r.width = s[0];
                let ad = 0;
                while (ad < r.width) {
                    ad += 64;
                }
                r.width = ad;
                r.height = s[1];
                ad = 0;
                while (ad < r.height) {
                    ad += 64;
                }
                r.height = ad;
            }
            return r;
        }
    },
    createWebuiRequest: function (config, node) {
        let res = {};
        const model = node[1];
        node = node[0];
        res.predictBody = {
            data: [model],
            fn_index: node.predict_index
        };
        if(config.image) {
            res.path = '/sdapi/v1/img2img';
            res.requestBody = {
                mode: 0,
                prompt: config.prompt,
                negative_prompt: config.negativePrompt,
                prompt_style: 'None',
                prompt_style2: 'None',
                init_images: [config.image],
                mask_mode: 'Draw mask',
                steps: config.steps,
                sampler_index: config.sampler,
                mask_blur: 4,
                inpainting_fill: 0,
                restore_faces: false,
                tiling: false,
                n_iter: 1,
                batch_size: 1,
                cfg_scale: config.scale,
                seed: config.seed,
                subseed: -1,
                subseed_strength: 0,
                seed_resize_from_h: 0,
                seed_resize_from_w: 0,
                seed_enable_extras: false,
                resize_mode: 0,
                inpaint_full_res: false,
                inpaint_full_res_padding: 32,
                inpaint_mask_invert: 'Inpaint masked',
                height: config.height,
                width: config.width,
                enable_hr: false,
                denoising_strength: 0.75,
                firstphase_width: 0,
                firstphase_height:0
            };
        }
        else {
            res.path = '/sdapi/v1/txt2img';
            res.requestBody = {
                prompt: config.prompt,
                negative_prompt: config.negativePrompt,
                prompt_style: 'None',
                prompt_style2: 'None',
                steps: config.steps,
                sampler_index: config.sampler,
                restore_faces: false,
                tiling: false,
                n_iter: 1,
                batch_size: 1,
                cfg_scale: config.scale,
                seed: config.seed,
                subseed: -1,
                subseed_strength: 0,
                seed_resize_from_h: 0,
                seed_resize_from_w: 0,
                seed_enable_extras: false,
                height: config.height,
                width: config.width,
                enable_hr: false,
                denoising_strength: 0.7,
                firstphase_width: 0,
                firstphase_height:0
            };
        }
        return res;
    },
    dateFormat: dateFormat
}

module.exports = kamiyaToolKit;