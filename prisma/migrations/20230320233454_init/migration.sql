/*
  Warnings:

  - Added the required column `height` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `model` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `negativePrompt` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `prompt` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `sampler` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `scale` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `seed` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `steps` to the `Image` table without a default value. This is not possible if the table is not empty.
  - Added the required column `width` to the `Image` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Image` ADD COLUMN `height` INTEGER NOT NULL,
    ADD COLUMN `model` VARCHAR(191) NOT NULL,
    ADD COLUMN `negativePrompt` VARCHAR(191) NOT NULL,
    ADD COLUMN `prompt` VARCHAR(191) NOT NULL,
    ADD COLUMN `sampler` VARCHAR(191) NOT NULL,
    ADD COLUMN `scale` INTEGER NOT NULL,
    ADD COLUMN `seed` INTEGER NOT NULL,
    ADD COLUMN `steps` INTEGER NOT NULL,
    ADD COLUMN `width` INTEGER NOT NULL;
