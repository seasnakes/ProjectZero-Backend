/*
  Warnings:

  - You are about to drop the column `height` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `model` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `negativePrompt` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `prompt` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `sampler` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `scale` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `seed` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `steps` on the `Image` table. All the data in the column will be lost.
  - You are about to drop the column `width` on the `Image` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Image` DROP COLUMN `height`,
    DROP COLUMN `model`,
    DROP COLUMN `negativePrompt`,
    DROP COLUMN `prompt`,
    DROP COLUMN `sampler`,
    DROP COLUMN `scale`,
    DROP COLUMN `seed`,
    DROP COLUMN `steps`,
    DROP COLUMN `width`;
