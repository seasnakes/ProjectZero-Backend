require('dotenv').config();

const express = require('express');
require('express-async-errors');
const cors = require('cors');
const expressJwt = require('express-jwt');

process.env.TZ = 'Asia/Shanghai';

const app = express();

app.use(cors({
    origin: ['https://www.kamiya.dev', 'https://www.closeai.us', ...(process.env.NODE_ENV !== 'production' ? ['http://127.0.0.1:11683', 'http://localhost:63342'] : [])],
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
}));

app.use(expressJwt.expressjwt({
    secret: process.env.JWT_SECRET,
    algorithms: ['HS256']
}).unless({
    path: ['/api/account/login', '/api/account/register', '/api/account/requestPasswordReset', '/api/account/resetPassword', '/api/account/verifyEmail', '/api/image/listBaseModel', '/api/image/listLoraModel', '/api/checkNetwork', '/api/billing/addCredit']
}));

app.get('/api/checkNetwork', (req, res) => {
    res.send({
        status: 200,
        message: 'OK'
    });
});

app.use(require('./routes/account'));

app.use(require('./routes/imageGenerate'));

app.use(require('./routes/sessionManage'));

app.use(require('./routes/userContent'));

app.use(require('./routes/migrateAccount'));

app.use(require('./routes/billing'));

app.use(require('./routes/kamiyaOpenAI'));

app.use(require('./routes/kamiyaGPTZero'));

app.listen(process.env.PORT || 11681, () => {
    console.log('Server is running on port ' + (process.env.PORT || 11681) + '.');
});
